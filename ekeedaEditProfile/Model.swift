//
//  Model.swift
//  ekeedaEditProfile
//
//  Created by MALLOJJALA PAVAN TEJA on 3/31/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation

struct EducationalDetails: Codable {
    let educationalDetails: [UserDetails]
}

struct UserDetails: Codable {
    let  registrationId: Int?
    let  StreamId: Int?
    let  StreamName: String?
    let  SemesterId: Int?
    let  SemesterName: String?
    let  MainStreamId: Int?
    let  MainStreamName: String?
    let  CollegeId: Int?
    let  CollegeName: String?
    let  CollegeCity: String?
    let  UniverstiyId: Int?
    let  UniversityName: String?
    let  OtherCollegeName: String?
    let  LevelOfCourseId: Int?
    let  LevelOfCourse: String?
    let  OtherUniversityId: Int?
    let  OtherUniversityName: String?
    let  OtherCollegeCity: String?
}


struct StreamModel: Codable {
    let streamId: Int
    let streamName: String
}

struct LevelOfCourseModel: Codable {
    let LevelOfCourseId: Int
    let LeveOfCourse: String
}

struct UniversityModel: Codable {
    let UniversityId: Int
    let UniversityName: String
}

struct BranchModel: Codable {
    let branchId: Int
    let branchName: String
}

struct UniversityBranchModel: Codable {
    let UniversityId: Int?
    let StreamId: String?
}

struct SemesterModel: Codable {
    let semesterId: Int
    let semesterName: String
}

struct CollegeModel: Codable {
    let CollegeId: Int
    let CollegeName: String
    let UniversityId: Int
    let UniversityName: String
    let LevelOfCourseId: Int
    let City: String
}



struct EducationModels: Codable {
    let streamModels:[StreamModel]
    let levelOfCourseModels: [LevelOfCourseModel]
    let collegeModels: [CollegeModel]
    let universityModels: [UniversityModel]
    let branchModels: [BranchModel]
    let semesterModels: [SemesterModel]
}

struct SavedResponse: Codable {
    let Status: Bool
    let Message: String
}
