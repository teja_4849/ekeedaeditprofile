//
//  ViewController.swift
//  ekeedaEditProfile
//
//  Created by MALLOJJALA PAVAN TEJA on 3/31/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var editProfilebtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
    }

    @IBAction func editProfileAction(_ sender: Any) {
        //editProfile
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let editProfileVC = storyBoard.instantiateViewController(withIdentifier: "editProfile") as! EditProfile
        
        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }
    
}

