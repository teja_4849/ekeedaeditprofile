//
//  progressSpinner.swift
//  ekeedaEditProfile
//
//  Created by MALLOJJALA PAVAN TEJA on 4/1/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation
import UIKit

fileprivate var aview: UIView?

extension UIViewController {
    
    func showSpinner()  {
        aview = UIView(frame: self.view.bounds)
        aview?.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView(style: .whiteLarge)
        ai.center = aview!.center
        ai.startAnimating()
        aview?.addSubview(ai)
        self.view.addSubview(aview!)
        
        Timer.scheduledTimer(withTimeInterval:  10.0, repeats: false) { (t) in
            self.removespinner()
        }
    }
    
    func removespinner()  {
        aview?.removeFromSuperview()
        aview = nil
        
    }
}
