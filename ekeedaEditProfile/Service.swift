//
//  Service.swift
//  ekeedaEditProfile
//
//  Created by MALLOJJALA PAVAN TEJA on 3/31/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation
final class Service: NSObject {
    
    //    // Create a singleton instance
    static let sharedInstance: Service = {
        return Service()
    }()
    
    
    private let baseUrl = "https://ekeeda.com/testapi/api/UserEducationalDetail"
    
    func fetchUsers( completionHandler: @escaping ([UserDetails]?, Error?) -> Void) {
        let url = URL(string: baseUrl)!
        print("fetching data for url: \(url)")

        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        let headers = [
            "RegId": "69619",
            "DeviceId": "08:25:25:25:74:1F"
        ]
        request.allHTTPHeaderFields = headers
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error {
                completionHandler(nil, error)
                print("Error with fetching users: \(error)")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    print("Error with the response, unexpected status code: \(String(describing: response))")
                    return
            }
            guard let data = data else {return}

            do {
                let userdetails = try JSONDecoder().decode(EducationalDetails.self, from: data)
                DispatchQueue.main.async {
                    
                    completionHandler(userdetails.educationalDetails, nil)
                }
            } catch let jsonErr{
                print("Failed to decode:", jsonErr)
            }
        })
        task.resume()
    }
    
    func fetchEducationDetails( completionHandler: @escaping (EducationModels?, Error?) -> Void) {
        let url = URL(string: baseUrl+"/lookUp")!
        print("fetching data for url: \(url)")
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let headers = [
            "RegId": "69619",
            "DeviceId": "08:25:25:25:74:1F"
        ]
        request.allHTTPHeaderFields = headers
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error {
                completionHandler(nil, error)
                print("Error with fetching users: \(error)")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    print("Error with the response, unexpected status code: \(String(describing: response))")
                    return
            }
            guard let data = data else {return}
//            print("yeay got response\(data)")
            do {
                let educationModel = try JSONDecoder().decode(EducationModels.self, from: data)
                DispatchQueue.main.async {
                    
                    completionHandler(educationModel, nil)
                }
            } catch let jsonErr{
                print("Failed to decode:", jsonErr)
            }
        })
        task.resume()
    }
    
    func saveUserDetails(parameters: [String: Any], completionHandler: @escaping (SavedResponse?, Error?) -> Void) {
        let url = URL(string: baseUrl+"/save")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)         } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let headers = [
            "RegId": "69619",
            "DeviceId": "08:25:25:25:74:1F"
        ]
        request.allHTTPHeaderFields = headers
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error {
                completionHandler(nil, error)
                print("Error with fetching users: \(error)")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    print("Error with the response, unexpected status code: \(String(describing: response))")
                    return
            }
            guard let data = data else {return}
            do {
                let savedResponse = try JSONDecoder().decode(SavedResponse.self, from: data)
                DispatchQueue.main.async {
                    
                    completionHandler(savedResponse, nil)
                }
            } catch let jsonErr{
                print("Failed to decode:", jsonErr)
            }
        })
        task.resume()
        
    }
}

