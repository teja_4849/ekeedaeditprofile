//
//  ViewModel.swift
//  ekeedaEditProfile
//
//  Created by MALLOJJALA PAVAN TEJA on 3/31/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation

struct UserDetailViewModel {
    
    let streamName: String
    let levelOfCourseName: String
    let collegeName: String
    let cityName: String
    let universityName: String
    let branchName: String
    let semesterName: String
    

    init(userDetails: UserDetails) {
        
        self.streamName = userDetails.StreamName ?? ""
        self.levelOfCourseName = userDetails.LevelOfCourse ?? ""
        self.collegeName = userDetails.CollegeName ?? ""
        self.cityName = userDetails.CollegeCity ?? ""
        self.universityName = userDetails.UniversityName ?? ""
        self.branchName = "branch name"
        self.semesterName = "\(userDetails.SemesterName ?? "")"
    }
    
}

struct EducationViewModel {
    private let eduModel: EducationModels

    
    init(eduModel: EducationModels) {
        self.eduModel = eduModel
    }
    
    public var streamNames: [StreamModel] {
        let f = eduModel.streamModels.map({$0})
        print(f)
        return eduModel.streamModels.map({$0})
    }
    
    public var leveOfCourseNames: [LevelOfCourseModel] {
        return eduModel.levelOfCourseModels.map({$0})
    }
    
    public var collegeNames: [CollegeModel] {
        return eduModel.collegeModels.map({$0})
    }
    
    public var universityNames: [UniversityModel] {
        return eduModel.universityModels.map({$0})
    }
    
    public var branchNames: [BranchModel] {
        return eduModel.branchModels.map({$0})
    }
    
    public var semesterNames: [SemesterModel] {
        return eduModel.semesterModels.map({$0})
    }
    
 
   

    
}
