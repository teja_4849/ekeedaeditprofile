//
//  EditProfile.swift
//  ekeedaEditProfile
//
//  Created by MALLOJJALA PAVAN TEJA on 3/31/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit

class EditProfile: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var editProfileTitle: UILabel!
    @IBOutlet weak var stream: UILabel!
    @IBOutlet weak var streamTf: UITextField!
    @IBOutlet weak var levelOfCourse: UILabel!
    @IBOutlet weak var levelOfCourseTf: UITextField!
    @IBOutlet weak var collegeName: UILabel!
    @IBOutlet weak var collegeNameTf: UITextField!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var cityNameTf: UITextField!
    @IBOutlet weak var universityName: UILabel!
    @IBOutlet weak var universityNameTf: UITextField!
    @IBOutlet weak var branchName: UILabel!
    
    @IBOutlet weak var branchNameTf: UITextField!
    @IBOutlet weak var semester: UILabel!
    @IBOutlet weak var semesterNameTf: UITextField!
    @IBOutlet weak var updateprofileBtn: UIButton!
    
    var userDetailsVM = [UserDetailViewModel]()
    weak var pickerView: UIPickerView?

    var streamNames = [StreamModel]()
    var levelOfCourseNames = [LevelOfCourseModel]()
    var collegeNames = [CollegeModel]()
    var universityNames = [UniversityModel]()
    var branchNames = [BranchModel]()
    var semesterNames = [SemesterModel]()
    public var params: [String: Any] = [:]

    
    override func viewDidLoad() {
        super.viewDidLoad()
     self.collegeNameTf.delegate = self
        self.collegeNameTf.addTarget(self, action: #selector(self.textFieldValueChanged(TextField:)), for: UIControl.Event.editingChanged)
        self.showSpinner()
        fetchData()
        loadPickers()
        createPicker()
        dismissKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    fileprivate func fetchData() {
        
        Service.sharedInstance.fetchUsers { (userdetails, err) in
            
            if let err = err {
                print("Failed to fetch courses:", err)
                return
            }
            DispatchQueue.main.async {
                self.userDetailsVM = userdetails?.map({UserDetailViewModel(userDetails: $0)}) ?? []
            self.loadFeilds()
                self.removespinner()
           }
        }
        
    }
    
    func loadFeilds () {
        if userDetailsVM.count > 0 {
            for userDetail in userDetailsVM {
        self.streamTf.text = userDetail.streamName
        self.levelOfCourseTf.text = userDetail.levelOfCourseName
        self.collegeNameTf.text = userDetail.collegeName
        self.cityNameTf.text = userDetail.cityName
        self.universityNameTf.text = userDetail.universityName
        self.branchNameTf.text = userDetail.branchName
        self.semesterNameTf.text = userDetail.semesterName
            }
        }
    }
    
    func loadPickers()  {
        Service.sharedInstance.fetchEducationDetails { (eduModels, err) in
            if let err = err {
                print("Failed to fetch courses:", err)
                return
            }
            DispatchQueue.main.async {
                let eduViewModel = EducationViewModel(eduModel: eduModels!)
                self.streamNames = eduViewModel.streamNames //list
                self.levelOfCourseNames = eduViewModel.leveOfCourseNames //list
                self.collegeNames = eduViewModel.collegeNames //list
                self.universityNames =  eduViewModel.universityNames
                self.branchNames = eduViewModel.branchNames //list
                self.semesterNames = eduViewModel.semesterNames //list

            }
        }
    }
    
 
    
    @objc func textFieldValueChanged(TextField:UITextField)
        
    {
        
        if((TextField.text?.count)!>0)
        {
//            self.collegeNames = (self.searchInArray(srchArray: self.collegeNames as! NSMutableArray, withKey: "key value", Characters: TextField.text!)) as! [String]
        }
        else
        {
            
           // self.filteredArray = self.collegeNames.mutableCopy() as! NSMutableArray
        }
        
      //  self.pickerView.reloadData()
        
        
    }
    func searchInArray(srchArray:NSMutableArray, withKey:String,Characters:String)->NSMutableArray
    {
        
        let resultArray=NSMutableArray()
        for index in 0..<srchArray.count
        {
            let Dict  = srchArray[index] as! [String:Any]
            if let stringmatcher = Dict[withKey] as? String
            {
                if(stringmatcher.contains(find: Characters))
                {
                    resultArray.add(Dict)
                    
                }
                else
                {
                    
                }
            }
            
        }
        
        return resultArray
        
    }
  
    // MARK: textfeild methods
    func createPicker()  {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        streamTf.inputView = picker
         levelOfCourseTf.inputView = picker
         collegeNameTf.inputView = picker
         branchNameTf.inputView = picker
         semesterNameTf.inputView = picker
        
        
        picker.backgroundColor = UIColor.white
        self.pickerView = picker
    }
    
    
    //MARK: handle Keyboard
    func dismissKeyboard()  {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(gesture:)))
        self.view.addGestureRecognizer(tap)
        
    }
    @objc func handleTap (gesture: UITapGestureRecognizer) {
        view.endEditing(true)
        
    }

    func save () {
        
        if let stream = streamTf.text, !stream.isEmpty, let course = levelOfCourseTf.text, !course.isEmpty, let college = collegeNameTf.text, !college.isEmpty, let city = cityNameTf.text, !city.isEmpty, let university = universityNameTf.text, !university.isEmpty, let branch = branchNameTf.text, !branch.isEmpty, let sem = semesterNameTf.text, !sem.isEmpty {
        
            let streamID =  self.streamNames.filter({$0.streamName == stream}).map({$0.streamId})
            params["StreamId"] = streamID.first
            let levelOfCourseId =  self.levelOfCourseNames.filter({$0.LeveOfCourse == course}).map({$0.LevelOfCourseId})
            params["LeveOfCourseId"] = levelOfCourseId.first
            let collegeId =  self.collegeNames.filter({$0.CollegeName == college}).map({$0.CollegeId})
            params["CollegeId"] = collegeId.first
            params["OtherCollegeName"] = college
            params["OtherCollegeCity"] = city
            let universityId =  self.universityNames.filter({$0.UniversityName == university}).map({$0.UniversityId})
            print("unis details \( self.universityNames.filter({$0.UniversityName == university}).map({$0}))")
            params["OtherUniversityId"] = universityId.first
            print("unis \(String(describing: universityId.first))")
            params["OtherUniversityName"] = university
            let mainStreamId =  self.streamNames.filter({$0.streamName == stream}).map({$0.streamId})
            params["MainStreamId"] = mainStreamId.first
            let semesterId =  self.semesterNames.filter({$0.semesterName == sem}).map({$0.semesterId})
            params["SemesterId"] = semesterId.first
            
            print("sending details", params, "count", params.count)

            Service.sharedInstance.saveUserDetails(parameters: params) { (response, err) in
                if let err = err {
                    print("Failed to fetch courses:", err)
                    return
                }
                self.removespinner()
                if  let response: Bool = response?.Status, response {
                    
                        let alertMessage = UIAlertController(title: "Education information updated successfully", message: "", preferredStyle: .alert)
                        alertMessage.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil
                        ))
                        self.present(alertMessage, animated: true, completion: nil)
                    
                }
            }

        } else {
            let alertMessage = UIAlertController(title: "missing feilds", message: "please fill all feilds to save profile", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil
            ))
            self.present(alertMessage, animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func updateProfileAction(_ sender: Any) {
        self.showSpinner()
        save()
    }
}

extension String {
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
}
}


extension EditProfile: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if streamTf.isFirstResponder {
            return streamNames.count
        } else if levelOfCourseTf.isFirstResponder {
            return levelOfCourseNames.count
        } else if collegeNameTf.isFirstResponder{
            return collegeNames.count
        } else if branchNameTf.isFirstResponder {
            return branchNames.count
        } else if semesterNameTf.isFirstResponder {
            return semesterNames.count
        }
        return 0
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if streamTf.isFirstResponder {
            let streams = streamNames.map({$0.streamName})
            return streams[row]
        } else if levelOfCourseTf.isFirstResponder {
            let courses = levelOfCourseNames.map({$0.LeveOfCourse})
            return courses[row]
        } else if collegeNameTf.isFirstResponder{
            let colleges = collegeNames.map({$0.CollegeName})
            return colleges[row]
        } else if branchNameTf.isFirstResponder {
            let branches = branchNames.map({$0.branchName})
            return branches[row]
        } else if semesterNameTf.isFirstResponder {
            let sems = semesterNames.map({$0.semesterName})
            return sems[row]
        }
        
        return ""

    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        if streamTf.isFirstResponder {
            let streamName = streamNames.map({$0.streamName})[row]
            streamTf.text = streamName
        } else if levelOfCourseTf.isFirstResponder {
            let levelOfCourseName =  levelOfCourseNames.map({$0.LeveOfCourse})[row]
            levelOfCourseTf.text = levelOfCourseName
        } else if collegeNameTf.isFirstResponder{
            let collegeName = collegeNames.map({$0.CollegeName})[row]
            collegeNameTf.text = collegeName
        } else if branchNameTf.isFirstResponder {
            let branchName = branchNames.map({$0.branchName})[row]
            branchNameTf.text = branchName
        } else if semesterNameTf.isFirstResponder {
            let semesterName = semesterNames.map({$0.semesterName})[row]
            semesterNameTf.text = semesterName
        }
        
}
}
